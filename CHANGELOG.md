# ChangeLog

<a name="v0.2.0"></a>
## [v0.2.0](https://gitlab.com/gpr-dev/templates/bumpversion-ci/compare/v0.1.0...v0.2.0)

> 2019-09-03

### Code Refactoring

* **bumpversion:** change default bumping commit message
* **gitlab-ci:** change tmp branch name
* **gitlab-ci:** move all variables in job
* **gitlab-ci:** use internal variable to get Gitlab host
* **gitlab-ci:** move install command in before_script

### Features

* **git-chglog:** add perf and merge commits sections
* **gitlab-ci:** update the ChangeLog before tagging
* **gitlab-ci:** generate the Gitlab release with git-chglog
* **vcs:** bump to next version after tag


<a name="v0.1.0"></a>
## v0.1.0

> 2019-09-01

### Features

* **vcs:** add job to bump version number and create tag

